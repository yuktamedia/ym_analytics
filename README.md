# ym_analytics

[![CI Status](https://img.shields.io/travis/devsane/ym_analytics.svg?style=flat)](https://travis-ci.org/devsane/ym_analytics)
[![Version](https://img.shields.io/cocoapods/v/ym_analytics.svg?style=flat)](https://cocoapods.org/pods/ym_analytics)
[![License](https://img.shields.io/cocoapods/l/ym_analytics.svg?style=flat)](https://cocoapods.org/pods/ym_analytics)
[![Platform](https://img.shields.io/cocoapods/p/ym_analytics.svg?style=flat)](https://cocoapods.org/pods/ym_analytics)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

ym_analytics is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'ym_analytics'
```

## Author

devsane, devsane@yuktamedia.com

## License

ym_analytics is available under the MIT license. See the LICENSE file for more info.
