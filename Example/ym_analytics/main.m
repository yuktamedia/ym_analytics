//
//  main.m
//  ym_analytics
//
//  Created by devsane on 12/24/2020.
//  Copyright (c) 2020 devsane. All rights reserved.
//

@import UIKit;
#import "YMAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([YMAppDelegate class]));
    }
}
