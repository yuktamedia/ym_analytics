//
//  YMAppDelegate.h
//  ym_analytics
//
//  Created by devsane on 12/24/2020.
//  Copyright (c) 2020 devsane. All rights reserved.
//

@import UIKit;

@interface YMAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
